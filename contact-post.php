<?php

$response = array("status" => "", "errors" => array());

if($_POST){
	$name = trim($_POST['name']);
	$humpledink = trim($_POST['humpledink']);
	$email = trim($_POST['email']);
	$message = trim($_POST['message']);

	if(!empty($name)){
		if(!preg_match("/^[a-zA-Z ]+$/", $name)) $response['errors']['name'] = "Invalid name.";
	} else $response['errors']['name'] = "Please enter your name.";

	if(!empty($humpledink)){
		if(strtolower($humpledink) != "no") $response['errors']['humpledink'] = "Robot!!!";
	} else $response['errors']['humpledink'] = "Indicate whether you're a robot or not.";

	if(!empty($email)){
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ $response['errors']['email'] = "Invalid Email."; }		
	} else $response['errors']['email'] = "Please enter your email.";

	if(!empty($message)){ 
		if(!preg_match("/^[a-zA-Z0-9\s:\.!\,\&;\(\)\'\"\$\@\)]+$/", $message)) $response['errors']['message'] = "No weird characters please.";
	} else $response['errors']['message'] = "Please enter a message.";

	if(!empty($_POST['phone'])) die();
	
	if (empty($response['errors'])) {	
		$sendto = 'oleosjo@gmail.com';
		
		$to = $sendto;
		$subject = "Volantis Contact Form Message";
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= 'From: ' . $name;
		
		mail($to,$subject,$message,$headers);
		
		$response['status'] = "success";
	}

} else {
    $response['status'] = "You didn't enter anything.";
}

exit(json_encode($response));

?>