<?php

$basedir = 'images/portfolio';
$list = listdir($basedir);
$dirs = $list['dirs'];     
$files = $list['files'];

$json = array("singles" => array());
$file_tree = array();

foreach ($dirs as $dir) {
    if($dir != $basedir){
        $folder = preg_grep('/' . preg_quote($dir, '/') . '/', $files);
        $files = array_diff($files, $folder);
        $file_tree[] = $folder;
    }
} 

foreach ($files as $file) {
    $json["singles"][] = array("file" => $file,
                        "caption" => "lorem"
                        );
}


$i = uniqid();
foreach ($file_tree as $dir) {
    foreach ($dir as $file) {
        $json[$i][] = array("file" => $file,
                        "caption" => "lorem");
    }
    $i = uniqid();
}
var_dump($json);


function listdir($dir='.') { 
    if (!is_dir($dir)) { 
        return false; 
    } 
    
    $files = array(); 
    $dirs = array();
    listdiraux($dir, $files, $dirs); 

    $stuff = array("files" => $files, "dirs" => $dirs);
    return $stuff; 
} 

function listdiraux($dir, &$files, &$dirs) { 
    $dirs[] = $dir;
    $handle = opendir($dir); 
    while (($file = readdir($handle)) !== false) { 
        if ($file == '.' || $file == '..') { 
            continue; 
        } 
        $filepath = $dir == '.' ? $file : $dir . '/' . $file; 
        if (is_link($filepath)) 
            continue; 
        if (is_file($filepath)) 
            $files[] = $filepath; 
        else if (is_dir($filepath)) 
            listdiraux($filepath, $files, $dirs); 
    } 
    closedir($handle); 
} 
asort($json);
file_put_contents("portfoliodata.json", json_encode($json));

?>