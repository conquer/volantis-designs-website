<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Volantis Designs</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- styles --> 
        <link rel="stylesheet" type="text/css" media="screen" href="bootstrap/css/bootstrap-custom.css">
        <link rel="stylesheet" type="text/css" media="screen" href="bootstrap/css/bootstrap-responsive.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="style.css">   
        <!-- fonts -->
        <link href='http://fonts.googleapis.com/css?family=Balthazar' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Rye' rel='stylesheet' type='text/css'>

        <!-- javascript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js" type="text/javascript"></script>
        <script src="script/masonry.pkgd.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="script/volantis.js"></script>

        <?php 
        $portfolio = json_decode(file_get_contents("portfoliodata.json"), true);
        foreach ($portfolio as $dir) {
          foreach ($dir as $file) {
            $next = $file['file'];
            echo '<link rel="prefetch" href="timthumb.php?src='.$next.'&w=619&h=500&s=1">'."\r\n"; 
          }
        }
        ?>
        <link rel="icon" href="images/favicon.ico">
    </head>
    <body>
      <!-- Google Analytics -->
        <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-42220424-1', 'volantisdesigns.com');
      ga('send', 'pageview');
      </script>
  <div class="container">
 <div class="row-fluid">
    <div class="span4 page-header">
        <h2 class="center">Welcome</h2>
        <p class="muted">We are a team of three, each with a specialized set of skills, ready to bring your identity to the web. We are a closely knit family... literally. We're all related! Together we can provide you with web sites, web apps, branding, graphic design, and beyond. </p>
    </div>
    <div class="span4 center center-column" id="logo">
        <img alt="" src="./images/volanty.png">
    </div>
    <div class="span4 page-header">  
         <h2 class="center">Our Quest</h2>
          <p class="muted">Volantis Designs is named after the flying fish constellation because it embodies our vision for being unique--the way a flying fish defies its norms. Our freelance team is here to help you rise above the ocean of mediocre design, into a sky of shining stars.
          </p>
    </div>
</div>
 <div class="row-fluid pad-top">
    <div class="span12">
        <h2 class="center" style="font-family:Skritchy; font-weight:bold; font-size:25px;">We are a web design company of three, ready to bring your identity to the web.</h2>
    </div>
</div>    
<div class="row-fluid pad-top">
  <div class="span4">
    <div class="character">
    <div class="speech-bubble steve">Hey I'm Steve.</div>
    <a href="#stevebio" data-toggle="modal">
    <img alt="" src="images/Steve1.png" id="steve">
     </a>
    <!-- Modal -->
      <div id="stevebio" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 id="myModalLabel">Stephen - Artist</h3>
        </div>
        <div class="modal-body">
          <p>Hi, Stephen Carr is my name. If you noticed Sarah and I share the same last name because we're married! Like Sarah, I have been drawing since I was a lad. Though not formally trained, I have been honing my skills with books and a ton of practice. Now I help with the initial stages of logo design and illustration work. In the time I am not drawing, I am working out or practicing Martial Arts, flips, and Parkour. I also am a coach of Parkour and a Martial Arts instructor. I like to wear many hats!</p>
        </div>
      </div>


</div>
  </div>
  <div class="span4">
    <div class="character">
    <div class="speech-bubble sae">Hey I'm Sae.</div>
      <a href="#saebio" data-toggle="modal">
      <img alt="" src="images/Sae1.png" id="sae">      
    </a>
      <div id="saebio" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 id="myModalLabel">Sae - Graphic Designer</h3>
        </div>
        <div class="modal-body">
          <p>I'm Sarah, but I often go by Sae. I've been drawing since I was just a kid: Not only with pencil on paper, but also on my awesome 90's wacom tablet. Today my self-taught style has evolved into a colorful, cute, vector explosion. Designing characters, logos, and typography are my favorites. In fact, typography is probably a perfect mesh of my two favorite things: design and writing (can we lump reading in there too?). I love to write, and work as a writing consultant at a graduate school, as well as hope to publish a novel or two in the future. Consequently, branding, copy, and marketing feel like second nature to me, and I get giddy brainstorming ways to present a business' unique message to world.</p>
          <p> <!-- You can usually find me hanging out with my artist/stuntman husband, 
            Stephen, and our two cats, Jane and Charlotte, (and I can't forget our 
            little dove, Clive!) in our Wake Forest, NC townhome. We love watching
             old Star Trek episodes, hunting for vintage furniture, and playing minecraft.  -->
             You can also find me here: <br> <a href="http://dribble.com/sae">dribble.com/sae</a> 
             <a href="http://instagram.com/saecarr">instagram.com/saecarr</a> 
            <a href="pinterest.com/saecarr">pinterest.com/saecarr</a> 
           </p>
        </div>
      </div>
  </div>
  </div>
  <div class="span4">
    <div class="character">
    <div class="speech-bubble jo">Hey I'm Jo.</div>
      <a href="#jobio" data-toggle="modal">
    <img alt="" src="images/Jo1.png" id="jo">    
    </a>  
    <div id="jobio" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
          <h3 id="myModalLabel">Jo - Programmer</h3>
        </div>
        <div class="modal-body">
          <p>Hello. My name is Jonathan and I am a web developer.
        I have been programming since I was a young lad and have a love of creating things through programming.
        My preeminent goal is to create clean, classy, standards-compliant
        websites while continuing to sharpen my coding skills, always open to, and eager to learn new things.
        Discussing philosophy,
        making the perfect cup of coffee, training parkour with my brother-in-law, and dancing on the weekends
        are some of my favorite pastimes.</p>
        </div>
      </div>
  </div>
  </div>
  </div>
</div>
<div class="row-fluid pad-top portfolio" style="margin-top:40px;">

  <div class="offset1 span11 portfolio-wall">
<div class="portfolio-wall-pic" style="width:200px;height166px;pading:10px;">
      <img src="images/PortfolioStatic.png" style="height:166px;">
    </div>
      <?php 
        foreach ($portfolio as $type => $dir): 
          if($type == "singles"): 
        foreach ($dir as $file): ?>
    <div class="portfolio-wall-pic"> <img class="thumb" width="200" height="166" src="timthumb.php?src=<?php if($_SERVER['HTTP_HOST'] == "localhost") echo "volantis/"; echo $file['file'] ?>&w=200&h=166&s=1">
         <div class="carousel">
                <div class="bigimg" style="background:url(timthumb.php?src=<?php if($_SERVER['HTTP_HOST'] == "localhost") echo "volantis/"; echo $file['file']; ?>&w=619&h=500&s=1) no-repeat"></div> 
<!--                 <div class="carousel-caption">
                 <p><?php echo $file['caption']; ?></p> 
               </div>  -->
             </div>
            </div>

      <?php endforeach; else: ?>
    <div class="portfolio-wall-pic"> <img class="thumb" width="200" height="166" src="timthumb.php?src=<?php if($_SERVER['HTTP_HOST'] == "localhost") echo "volantis/"; echo $dir[0]['file'] ?>&w=200&h=166&s=1">
        <?php $id = uniqid(); ?>
       <div id="<?php echo $id ?>" class="carousel slide">
          <ol class="carousel-indicators">
            <?php $i = 0; 
            foreach ($dir as $file): ?>
            <li data-target="#<?php echo $id ?>" data-slide-to="<?php echo $i ?>" <?php if($i == sizeof($dir) -1): ?> class="active"<?php endif; ?>></li>
            <?php $i++; endforeach; ?>
          </ol>
          <div class="carousel-inner">
            <?php $i = 0; 
            foreach ($dir as $file): ?>
              <div class="item<?php if($i == sizeof($dir) -1) echo " active"; ?>">
                <div class="bigimg" style="background:url(timthumb.php?src=<?php if($_SERVER['HTTP_HOST'] == "localhost") echo "volantis/"; echo $file['file']; ?>&w=619&h=500&s=1) no-repeat"></div> 
       <!--          <div class="carousel-caption">
                 <p><?php echo $file['caption']; ?></p> 
               </div>  -->
             </div>
            <?php $i++; endforeach; ?>
            </div>
        </div>
      </div>

    <?php endif; endforeach; ?>
      
  </div>
</div>
<div class="row-fluid pad-top bottom-section" style="padding-top:20px;">
  <div class="offset4 span4">
      <form id="contact" action="./contact-post.php" method="post">
         <fieldset>
          <legend><h2 class="center">Contact Us</h2></legend>
          <div id="success-message" class="alert alert-success hide">
              <button type="button" class="close" onclick="$(this).parent().fadeOut();">&times;</button>
              <strong>Message Sent!</strong> We'll get back to you as soon as possible.
            </div>
          <div class="row-fluid">
          <div class="span7">
              <input name="phone" type="hidden" value="">
             <label>Name</label>
             <input id="name" name="name" class="span10" type="text" placeholder="Name">
             <label>Email</label>
             <input id="email" name="email" class="span10" type="text" placeholder="Email">
           </div>
           <div class="span4 visible-desktop visible-tablet" style="position:relative">
             <img alt="" style="position:absolute; top:20px;" src="images/MailBirdL.png">
           </div>
          </div>
          <div class="row-fluid">
            <div class="span12">
             <label>Message</label>
             <textarea id="message" name="message" class="span12" rows="5"></textarea>
             <label>Are you a robot?
             <input id="humpledink" name="humpledink" class="span2" type="text"></label>
             <input type="submit" class="btn btn-block btn-volantis">
         </fieldset> 
     </form>
   </div>
  <!-- </div> -->
</div>
  </div>
</div>
<div style="padding:20px; padding-top:40px" class="center">
</div>    
    </body>
</html>