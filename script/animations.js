$(document).ready(function(){
	$(".fadein").fadeIn("slow");

var $container = $('#portfolio-wall');
$container.imagesLoaded(function(){
  $container.masonry({
    itemSelector : '.wallimg',
    columnWidth : 140
  });
});


$(".sae").hover(
        function() {
        $(this).find(".bottom").css("opacity", "100");
          $('#arrows').animate({
              left: '-30',
            }, {duration: 500, queue: false});

        },
        function () {
        $(this).find(".bottom").css("opacity", "0");
          $('#arrows').animate({
              left: '0',
            }, {duration: 500, queue: false});
        });

$(".jo").hover(
        function() {
        $(this).find(".bottom").css("opacity", "100");
          $('#arrows').animate({
              left: '30',
            }, {duration: 500, queue: false});

        },
        function () {
        $(this).find(".bottom").css("opacity", "0");
          $('#arrows').animate({
              left: '0',
            }, {duration: 500, queue: false});
        });

});