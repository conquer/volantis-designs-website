$(document).ready(function(){

/*====================================
=            Contact Form            =
====================================*/

  $("#name, #email, #message, #humpledink").on('keypress', function() {
      $(this).popover('destroy');
  });

  $(window).on('resize', function(e) {
      $('#name').popover('destroy');
      $('#email').popover('destroy');
      $('#message').popover('destroy');
      $('#humpledink').popover('destroy');
  });

  $('#contact').on('submit', function(e) {
      e.preventDefault();  

      $('#name').popover('destroy');
      $('#email').popover('destroy');
      $('#message').popover('destroy');
      $('#humpledink').popover('destroy');

      $.post("contact-post.php", $("#contact").serialize(), function (data) {
        var response = $.parseJSON(data);

        if(typeof response.errors !== undefined){
          $('#name').popover({placement: "left", trigger:"manual",content: response.errors.name});
          $('#name').popover('show');

          $('#email').popover({placement: "left", trigger:"manual",content: response.errors.email});
          $('#email').popover('show');

          $('#message').popover({placement: "left", trigger:"manual",content: response.errors.message});
          $('#message').popover('show');

          $('#humpledink').popover({placement: "right", trigger:"manual",content: response.errors.humpledink});
          $('#humpledink').popover('show');
        }

        if(response.status == "success") {  
          $('#success-message').fadeIn();
          $('#name, #email, #message, #humpledink').val("");
        }
        });
      return false;
  });


/*======================================
=            Image Hovering            =
======================================*/

$("#logo_bio").hover(
  function() {
    $(this).attr("src", "./images/logo_bio_hover.png");
  },
  function() {
    $(this).attr("src", "./images/logo_bio.png");
  });

  // Preload Sae and Jo hover Images
  sae = $("<img/>").src = "images/Sae2.png";
  jo = $("<img/>").src = "images/Jo2.png";
  steve = $("<img/>").src = "images/Steve2.png";


$("#sae").hover(
        function() {
          $("#sae").attr("src", "images/Sae2.png"); 
        },
        function () {
          $("#sae").attr("src", "images/Sae1.png"); 
        });

$("#jo").hover(
        function() {
          $("#jo").attr("src", "images/Jo2.png"); 
        },
        function () {
          $("#jo").attr("src", "images/Jo1.png"); 
        });

$("#steve").hover(
        function() {
          $("#steve").attr("src", "images/Steve2.png"); 
        },
        function () {
          $("#steve").attr("src", "images/Steve1.png"); 
        });

/*======================================
=            Portfolio Wall            =
======================================*/

var $container = $('.portfolio-wall');
// layout Masonry again after all images have loaded
$container.imagesLoaded( function() {
  $container.masonry({
    itemSelector: '.portfolio-wall-pic',
    isAnimated: true
  });
});
    $('.carousel').carousel({
      interval: 2000
    });

$lastClicked = null;

$(".portfolio-wall-pic .thumb").click(function() {
  if($lastClicked)  {  
    $lastClicked.css({
      "width": 200 ,
      "height": 'auto'});
    $lastClicked.children(".thumb").fadeIn();
    $lastClicked.children(".carousel").fadeOut();
  }

  $clicked = $(this).parent();

  // $clicked.find(".bigimg").each(function() {
  //   $(this).attr("src", $(this).attr("data"));
  // })

  $clicked.css({
    "width": 600,
    "height": 500
  });

  $clicked.children(".carousel").fadeIn();
  $clicked.children(".thumb").fadeOut();

  $container.masonry("layout");
  $lastClicked = $clicked;
});


$("#logo img").dblclick(function() {
    $ff = $("<img src='images/ff.gif'/>");
     $("#logo").css({  
          '-webkit-transform': 'rotateY(180deg)',
    '-moz-transform': 'rotateY(180deg)',
    '-ms-transform': 'rotateY(180deg)',
    '-o-transform': 'rotateY(180deg)',
    'transform': 'rotateY(180deg)'}).one('transitionend webkitTransitionEnd oTransitionEnd otransitionend', function() {
         $("#logo").html($ff);
         $("#logo").css({  
          '-webkit-transform': 'rotateY(0deg)',
          '-moz-transform': 'rotateY(0deg)',
          '-ms-transform': 'rotateY(0deg)',
          '-o-transform': 'rotateY(0deg)',
          'transform': 'rotateY(0deg)'});
      });
})

});
