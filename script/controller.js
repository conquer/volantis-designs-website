
var volantis = angular.module('volantis', ['ng']).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  	  when('/', {templateUrl: 'home.html', controller: homeCtrl}).
      when('/jo', {templateUrl: 'portfolio.html',   controller: portfolioCtrl}).
      when('/sae', {templateUrl: 'portfolio.html', controller: portfolioCtrl}).
      when('/sae/:portid', {templateUrl: 'portfolio.html', controller: portfolioCtrl}).
      otherwise({redirectTo: '/'});
}]);


function homeCtrl($scope, $routeParams) {
	$(".jo, .sae").hover(
            function() {
            $(this).find(".bottom").css("opacity", "100");
            },
            function () {
            $(this).find(".bottom").css("opacity", "0");
            });
}

function portfolioCtrl($scope, $http, $routeParams) {

    $scope.portid = $routeParams.portid;
  $http({method: 'GET', url: 'data/sae-portfolio.json'}).
  success(function(data, status, headers, config) {
      $scope.portfolio = data;
  });

}

function portfolioDetailCtrl($scope, $http, $routeParams) {
}

