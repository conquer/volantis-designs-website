<?php include("header.php") ?>

 <div class="row-fluid">
            <div class="span2 center">
              <a href="./sae.php">
                        <img src="./images/bio_apple.png">
                </a>                     
                    </div>
                    <div class="span8 center-column page-header">
                        <h1>Sae's Portfolio <br>
                            <small>Golden Delicious Designs.</small></h1>
                        
                    </div>
                    <div class="span2 center pad-top">
                      <a href="./">
                      <img  src="./images/logo_bio.png" id="logo_bio">
                    </a>
                    </div>
            </div>
            <div class="row-fluid">
                <div class="span2 pad-top">
                <p>
Hullo! I'm Sarah, but I often go by Sae. I've been drawing since I was just a kid: both magical unicorns with pencil on paper and sweet Microsoft Paint creations on my awesome 90's wacom tablet. Today my self-taught style has evolved into a colorful, cute, vector explosion. Designing characters is one of my favorite things ever, but I also love the victory of a crisp, clean logo, or some delicious typography. In fact, typography is probably a perfect mesh of my two favorite things: design and writing (can we lump reading in there too?). I love to write, and work as a writing consultant at a graduate school, as well as hope to publish a novel or two in the future. Consequently, branding, copy, and marketing feel like second nature to me, and I get giddy brainstorming ways to present a business' unique message to world. 
</p>
<p>
You can usually find me hanging out with my stuntman husband, Stephen, and our two cats, Jane and Charlotte, (and I can't forget our little dove, Clive!) in our Wake Forest, NC townhome. We love watching old Star Trek episodes, hunting for vintage furniture, and playing minecraft. You can also find me here:                  </p>
</p>
                </div>
                <?php 
                $json = file_get_contents("./data/sae-portfolio.json");
                $json = json_decode($json);

                if(!isset($_GET['p'])): ?>
                    <div class="span7 fadein hide">
                    <div id="#portfolio-wall">
               <?php 
                    foreach ($json as $project):
                    $thumbimg = str_replace("../", "./", $project->images[0]->thumb);
                 ?>      
                    <div class="wallimg">
                        <a href="?p=<?php echo array_search($project, $json); ?>"><img src="<?php echo $thumbimg ?>"></a>
                    </div>   

                <?php endforeach; ?>
                    </div>
                </div>
                <?php else: 
                    $project = $json[$_GET['p']];
                    $first = true;
                ?>
                <div class="span8 fadein hide">
                    <div id="projectCarousel" class="carousel slide">
<!--                       <ol class="carousel-indicators">
                        <li data-target="#projectCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#projectCarousel" data-slide-to="1"></li>
                        <li data-target="#projectCarousel" data-slide-to="2"></li>
                      </ol> -->
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <?php 
                        foreach($project->images as $img): 
                            if ($img != null):
                            $imgFile = str_replace("../", "./", $img->file);
                            ?>
                        <div class="item <?php if($first) echo 'active' ?>">
                            <img src="<?php echo $imgFile ?>">
                            <div class="carousel-caption">
                              <h4><?php echo $img->title ?></h4>
                              <p><?php echo $img->description ?></p>
                            </div>
                        </div>
                        <?php 
                        $first = false;
                        endif; endforeach; ?>
                      </div>
                      <!-- Carousel nav -->
                      <a class="carousel-control left" href="#projectCarousel" data-slide="prev">&lsaquo;</a>
                      <a class="carousel-control right" href="#projectCarousel" data-slide="next">&rsaquo;</a>
                    </div>
                </div>
                <?php endif; ?>

            </div>
</div>

<?php include("footer.php"); ?>