<?php include("header.php") ?>
<script>
$(document).ready(function(){
    $(".expand-button").click(function(){
        $(this).parent().parent().animate({
            width: '200%',
            height: '200%',
            left: "-=50%"
          }, 500, function() {
            // Animation complete.
          });
    });
});
</script>
<div class="row-fluid">
    <div class="span2 center" id="johead">
        <img src="./images/jopixel.png">
    </div>
    <div class="span8 center-column page-header" style="margin-top:0;">
        <h1>Jo's Portfolio <br>
        <small>HTML5 | CSS3 | jQuery | PHP</small></h1>
        
    </div>
    <div class="span1 center pad-top" id="logo_bio_container">
        <a href="./">
        <img  src="./images/logo_bio.png" id="logo_bio">
        </a>
    </div>
</div>
<div class="row-fluid">
    <div class="span2">
        <div class="link-icons">
            <a href="http://www.linkedin.com/pub/jonathan-mcdill/64/557/a9b"><img src="images/icon_linkedin.png" title="My LinkedIn Profile"></a>
            <a href="http://github.com/oleosjo"><img src="images/icon_github.png" title="My Github page"></a>
        </div>
        <p class="muted" style="font-style:oblique; padding-top:10px;">
        Hello. My name is Jonathan and I am a web developer.
        I have been programming since he was a young lad and have a real love of fabricating immaculate code.
        My preeminent goal is to create clean, classy, standards-compliant
        websites while continuing to sharpen my coding skills, always open to, and eager to learn new things.
        Discussing philosophy,
        making the perfect cup of coffee, training parkour with my brother-in-law, and dancing on the weekends
        are some of my favorite pastimes.
        </p>
    </div>
    <div class="span8 jo-portfolio">
        <div class="row-fluid" style="padding-bottom:20px;">
            <div class="span6">
                <div class="flipcontainer">
                    <div class="flipcard">
                        <div class="face">
                            <img class="portfolio-pic" src="thumber.php?img=images/jo-portfolio/wfp.png&w=400&h=300" alt="">
                        </div>
                        <div class="face back">
                            <h3>Wake Forest Parkour</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. is deleniti ut rem id. Dolorum, voluptates.</p>
                            <p><a href="http://www.wakeforestparkour.com">wakeforestparkour.com</a></p>
                            <p><button class="expand-button btn-inverse">More</button></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="flipcontainer">
                    <div class="flipcard">
                        <div class="face">
                            <img class="portfolio-pic" src="thumber.php?img=images/jo-portfolio/theisland.png&w=400&h=300" alt="">
                        </div>
                        <div class="face back">
                            <h3>The Island</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. is deleniti ut rem id. Dolorum, voluptates.</p>
                            <p><a href="http://www.kongregate.com/games/oleosjo/the-island">kongregate.com/games/oleosjo/the-island</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php include("footer.php"); ?>