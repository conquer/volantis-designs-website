var volantis = angular.module('admin', ['ngUpload']).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  	  when('/', {templateUrl: 'partials/home.html', controller: adminCtrl}).
      when('/edit/:pid/:imgid', {templateUrl: 'partials/edit.html',   controller: adminCtrl}).
      when('/edit/:pid', {templateUrl: 'partials/editproj.html',   controller: adminCtrl}).
      when('/upload/:pid', {templateUrl: 'partials/upload.html',   controller: uploadCtrl}).
      otherwise({redirectTo: '/'});
}]);

function uploadCtrl($scope, $routeParams, $rootScope){

  $scope.add_image = function (pid) {
    
    $rootScope.current_proj.images.push(      {
        "id": uniqid(),
        "file": $scope.response.url,
        "thumb": "thumber.php?img="+$scope.response.url+"&w=100&h=100",
        "url": "",
        "description": "",
        "title": ""
      })
  }

  $scope.results = function(content, completed) {
    $scope.response = Array();
    if (completed && content.length > 0) {
      $scope.response = JSON.parse(content);
      $scope.add_image($routeParams.pid);
    }
    else {
      $scope.response.status = "Uploading...";
    }
  }

}

function adminCtrl($scope, $http, $routeParams, $location, $rootScope, $timeout){

 if(!$rootScope.portfolio) {
    $http({method: 'GET', url: '../data/sae-portfolio.json'}).
    success(function(data, status, headers, config) {
        $rootScope.portfolio = data;
        $rootScope.tab = $rootScope.portfolio[0].id;
        $rootScope.$watch('tab', function(newVal) {
              $rootScope.current_proj = $scope.get_proj($rootScope.tab);
        })
	  });
      
	}

  $scope.pid = $routeParams.pid;
  $scope.imgid = $routeParams.imgid; 

  $scope.get_proj = function(id) {
      index = findWithAttr($rootScope.portfolio, "id", id)
      return $rootScope.portfolio[index];
  }

  $scope.currentTab = function(index) {
    if($rootScope.tab == index) {
      return 'active';
    }
  }

  $scope.changeTab = function(index) {
    $rootScope.tab = index;
  }

  $scope.saveEdit = function() {
    $location.path( "/" );
    $rootScope.message = '';

  }

  $scope.commitChanges = function() {
  	 $http({method: 'POST', url: './api.php', data:{"action":"save_data", "data":$rootScope.portfolio} }).
	  success(function(data, status, headers, config) {
	      $rootScope.message = data;
        $timeout(function(){ $rootScope.message = ""},2000, true);
	  });
  }

  $scope.moveProj = function(moveTo) {
      $rootScope.portfolio[moveTo].images.push($rootScope.portfolio[$routeParams.pid].images[$routeParams.imgid]);
      $rootScope.portfolio[$routeParams.pid].images.splice($routeParams.imgid,1);
      $location.path( "/" );
  }

  $scope.newProject = function() {

      $rootScope.portfolio.unshift({
          "name" : "New Project",
          "id" : uniqid(),
          "description": "",
          "url": "",
          "images":[] 
        });
  }

  $scope.delProj = function () {
    i = $rootScope.portfolio.indexOf($rootScope.current_proj);
    $rootScope.portfolio.splice(i, 1);
    $rootScope.tab = $rootScope.portfolio[0].id;
    $location.path( "/" );
  }

  $scope.delImg = function () {
      $rootScope.portfolio[$routeParams.pid].images.splice($routeParams.imgid,1);
      $location.path( "/" );
  }

}

function findWithAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
}

function uniqid (prefix, more_entropy) {
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +    revised by: Kankrelune (http://www.webfaktory.info/)
  // %        note 1: Uses an internal counter (in php_js global) to avoid collision
  // *     example 1: uniqid();
  // *     returns 1: 'a30285b160c14'
  // *     example 2: uniqid('foo');
  // *     returns 2: 'fooa30285b1cd361'
  // *     example 3: uniqid('bar', true);
  // *     returns 3: 'bara20285b23dfd1.31879087'
  if (typeof prefix === 'undefined') {
    prefix = "";
  }

  var retId;
  var formatSeed = function (seed, reqWidth) {
    seed = parseInt(seed, 10).toString(16); // to hex str
    if (reqWidth < seed.length) { // so long we split
      return seed.slice(seed.length - reqWidth);
    }
    if (reqWidth > seed.length) { // so short we pad
      return Array(1 + (reqWidth - seed.length)).join('0') + seed;
    }
    return seed;
  };

  // BEGIN REDUNDANT
  if (!this.php_js) {
    this.php_js = {};
  }
  // END REDUNDANT
  if (!this.php_js.uniqidSeed) { // init seed with big random int
    this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
  }
  this.php_js.uniqidSeed++;

  retId = prefix; // start with prefix, add current milliseconds hex string
  retId += formatSeed(parseInt(new Date().getTime() / 1000, 10), 8);
  retId += formatSeed(this.php_js.uniqidSeed, 5); // add seed hex string
  if (more_entropy) {
    // for more entropy we add a float lower to 10
    retId += (Math.random() * 10).toFixed(8).toString();
  }

  return retId;
}