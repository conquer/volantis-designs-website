<?php

// $json_file = json_decode(file_get_contents("./data/sae-portfolio.json"));

$files = listdir('../images/sae-portfolio/'); 

$json = array(array('name' => 'Misc',
            'description' => 'Project Description',
			'url' => 'http://cool.com',
			'images' =>  array()));

foreach ($files as $file) {

    // generate template
    if ($file !== null) {
    array_push($json[0]['images'], array('id' => uniqid(),
                            'file' => $file,
                            'thumb' => "thumber.php?img=".$file."&w=100&h=100",
                            'url' => '',
                            'description' => '',
                            'title' => ''));
	}	
}



function listdir($dir='.') { 
    if (!is_dir($dir)) { 
        return false; 
    } 
    
    $files = array(); 
    listdiraux($dir, $files); 

    return $files; 
} 

function listdiraux($dir, &$files) { 
    $handle = opendir($dir); 
    while (($file = readdir($handle)) !== false) { 
        if ($file == '.' || $file == '..') { 
            continue; 
        } 
        $filepath = $dir == '.' ? $file : $dir . '/' . $file; 
        if (is_link($filepath)) 
            continue; 
        if (is_file($filepath)) 
            $files[] = $filepath; 
        else if (is_dir($filepath)) 
            listdiraux($filepath, $files); 
    } 
    closedir($handle); 
} 

file_put_contents("../data/sae-portfolio.json", json_encode($json));
// $request_body = file_get_contents('php://input');
// if ($request_body != "")
// if(file_put_contents("../data/sae-portfolio.json", $request_body)){
//     echo "Saved.";
// }



?>