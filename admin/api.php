<?php

$possible_actions = array("save_data", "walk_dir");

if (isset($_GET['action']))
	$action = $_GET['action'];
elseif (isset($POST['action']))
	$action = $POST['action'];
elseif ($request_body = json_decode(file_get_contents('php://input'), true)) {
	$action = $request_body['action'];
}
$response = array("error" => "An unknown error has occured.");

if(isset($action) && in_array($action, $possible_actions)) {
	switch ($action) {
		case 'save_data':
			$response = save_data($request_body);
			break;
		case 'walk_dir':
			$response = walk_dir();
			break;
	}
}

function save_data($request_body) {
	if ($request_body != ""){
		if(file_put_contents("../data/sae-portfolio.json", json_encode($request_body['data']))){
			return array("status" => "Successfully Saved");	
		}
	} else {
		return array("status" => "failed");
	}
}

function walk_dir() {

// $json_file = json_decode(file_get_contents("./data/sae-portfolio.json"));

$files = listdir('../images/sae-portfolio/'); 

$json = array(array('name' => 'Misc',
			'id' => uniqid(),
            'description' => 'Project Description',
			'url' => 'http://cool.com',
			'images' =>  array()));

foreach ($files as $file) {

    // generate template
    if ($file !== null) {
    array_push($json[0]['images'], array('id' => uniqid(),
                            'file' => $file,
                            'thumb' => "thumber.php?img=".$file."&w=100&h=100",
                            'url' => '',
                            'description' => '',
                            'title' => ''));
		}	
	}

if(file_put_contents("../data/sae-portfolio.json", json_encode($json))){
	return array("status" => "done.");
}
	
}

function listdir($dir='.') { 
    if (!is_dir($dir)) { 
        return false; 
    } 
    
    $files = array(); 
    listdiraux($dir, $files); 

    return $files; 
} 

function listdiraux($dir, &$files) { 
    $handle = opendir($dir); 
    while (($file = readdir($handle)) !== false) { 
        if ($file == '.' || $file == '..') { 
            continue; 
        } 
        $filepath = $dir == '.' ? $file : $dir . '/' . $file; 
        if (is_link($filepath)) 
            continue; 
        if (is_file($filepath)) 
            $files[] = $filepath; 
        else if (is_dir($filepath)) 
            listdiraux($filepath, $files); 
    } 
    closedir($handle); 
}

exit(json_encode($response));

?>