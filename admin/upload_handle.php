<?php
	$uploaddir = './upload/';
	$uploadfile = $uploaddir . basename($_FILES['imagefile']['name']);

	$response = array();
	$filetypes = array("image/gif","image/jpeg","image/jpg","image/png");

	if (!in_array($_FILES["imagefile"]["type"], $filetypes)) {
		$response['status'] = "That's not an image file";
		die(json_encode($response));
	} 

	if ($_FILES["imagefile"]["size"] > 5242880 ) {
		$response['status'] = "Uhhh... that's too big";
		die(json_encode($response));
	}

	if (move_uploaded_file($_FILES['imagefile']['tmp_name'], $uploadfile)) {
	    $response['status'] = "Successfully uploaded.";
	    $response['url'] = $uploadfile;

	} else {
	    $response['status'] = "Error: File not uploaded.";
	}

	echo json_encode($response);
?>