<?php
// In PHP versions earlier than 4.1.0, $HTTP_POST_FILES should be used instead
// of $_FILES.

$uploaddir = './upload/';
$uploadfile = $uploaddir . basename($_FILES['imagefile']['name']);

$response = array();
$filetypes = array("image/gif","image/jpeg","image/jpg","image/png");

if (!in_array($_FILES["imagefile"]["type"], $filetypes)) {
	$response['message'] = "That's not an image file";
	die(json_encode($response));
} 

if ($_FILES["imagefile"]["size"] > 5242880 ) {
	$response['message'] = "Uhhh... that's too big";
	die(json_encode($response));
}

if (move_uploaded_file($_FILES['imagefile']['tmp_name'], $uploadfile)) {
    $response['message'] = "Successfully uploaded.";
    $response['url'] = $uploadfile;

} else {
    $response['message'] = "Error: File not uploaded.";
}

echo json_encode($response);

?>